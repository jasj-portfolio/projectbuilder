from setuptools import setup, find_packages
import codecs
import os

here = os.path.abspath(os.path.dirname(__file__))

with codecs.open(os.path.join(here, "README.md"), encoding="utf-8") as fh:
    long_description = "\n" + fh.read()

VERSION = '0.9.5'
DESCRIPTION = 'A Python project builder.'
LONG_DESCRIPTION = "Python project builder not quite as functional as cargo, but I'm working on it"

# Setting up
setup(
    name="projectcreator",
    version=VERSION,
    author="johnnytfg (Johnny Scott)",
    author_email="unavailable at the moment",
    description=DESCRIPTION,
    long_description_content_type="text/markdown",
    long_description=long_description,
    packages=find_packages(),
    install_requires=['pyinstaller', 'pipreqs'],
    keywords=['python', 'project', 'project builder', 'project creator'],
    classifiers=[
        "Development Status :: 1 - Planning",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "Operating System :: Unix",
        "Operating System :: Microsoft :: Windows",
    ]
)