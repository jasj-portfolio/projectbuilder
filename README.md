# Project Builder
- Version 0.9.5

## About
Project Builder is a tool to help maintain Python projects. Still in Alpha phase it does not have a lot of features at the moment. Development will be slow I'm working on it when I'm not busy with work or school. Currently available for GNU/Linux and Windows.

## Installation
- git clone https://www.gitlab.com/jasj-portfolio/projectbuilder.git
- cd projectbuilder/src
- chmod +x pb.py
- cp pb.py ~/.local/bin (GNU/Linux)
- or
- git clone https://www.gitlab.com/jasj-portfolio/projectbuilder.git
- cd projectbuilder/src
- chmod +x pb.py
- add the directory where pb.py is located to your PATH (Windows)

## Current Features
- init: creates your project (untested on Windows)
- dep: creates a requirements.txt so you have a list of dependencies and their versions (untested on Windows)
- build: uses pyinstaller to create an executable file for your project (untested on Windows)
- destroy: deletes your project (untested on Windows)
- help: displays options currently available with project builder (untested on Windows)

# How To
- When putting in project-name you will not need a file extension, project-name is refering to the project directory.
- help can be ran from anywhere

```
../project-dir <= run init, destroy from here
    |-/project-dir <= run build, dep from here
        |-__init__.py
        |-setup.py
        |-README.md
        |-/src
            |-__init__.py
            |-main.py
```

## Start a Project
- pb.py init project-name

## Build an Executable
- pb.py build project-name

## Get Dependencies
- pb.py dep project-name

## Delete a Project
- pb.py destroy project-name

## Help
- pb.py help