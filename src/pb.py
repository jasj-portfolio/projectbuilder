#!/usr/bin/env python3

# Project Builder is a project I made for school,
# I made it to make setting up projects easier.
# Currently this project is still in Alpha and new features
# are being added.

from sys import argv
import os
import shutil

class ProjectCreator:

    def __init__(self, project):
        self.project = project

    # init creates project files and the file structure of the project
    def init(self):
        x = ["__init__.py", "README.md", ".gitignore", "setup.py"]
        src = ["__init__.py", "main.py"]

        parent_dir = os.getcwd()
        project_dir = self.project
        src_dir = "src"

        path1 = os.path.join(parent_dir, project_dir)
        path2 = os.path.join(parent_dir, project_dir, src_dir)
        
        try:
            os.mkdir(path1)
            os.mkdir(path2)

            for i in x:
                files = os.path.join(path1, i)
                with open(files, "w") as create:
                    create.close()

            for i in src:
                files = os.path.join(path2, i)
                with open(files, "w") as create:
                    create.close()

            # setup variable holds the file contents for setup.py
            setup = '''
            from setuptools import setup, find_packages
            import codecs
            import os

            here = os.path.abspath(os.path.dirname(__file__))

            with codecs.open(os.path.join(here, "README.md"), encoding="utf-8") as fh:
                long_description = "\\n" + fh.read()

            VERSION = '0.1.0'
            DESCRIPTION = 'Simple interface for working with intents and chatbots.'
            LONG_DESCRIPTION = 'Simple interface for working with intents and chatbots.'

            # Setting up
            setup(
                name="",
                version=VERSION,
                author="",
                author_email="",
                description=DESCRIPTION,
                long_description_content_type="text/markdown",
                long_description=long_description,
                packages=find_packages(),
                install_requires=[],
                keywords=[],
                classifiers=[

                ]
            )
            '''
            # adds content to setup.py
            setup_file = os.path.join(path1, "setup.py")
            with open(setup_file, 'a') as content:
                content.writelines(setup)

            # adds a title to README.md
            readme = os.path.join(path1, "README.md")
            with open(readme, 'a') as content:
                content.write(f'# {self.project}')
            
            # let's user know when the project has been created
            print("Project has been created.")
        except:
            print("Was unable to create project")

    # creates an executable of your project (feature currently untested)
    def build(self):
        try:
            parent_dir = os.getcwd()
            built = os.path.join(parent_dir, 'src/main.py')
            os.system(f'pyinstaller --onefile --windowed {built}')
        except:
            print(f'Please run in the {self.project} directory')

    # creates a requirements.txt file so you can see which version of the packages
    # are used in your project (feature currently untested)
    def dependencies(self):
        try:
            parent_dir = os.getcwd()
            dep = os.path.join(parent_dir, 'src')
            os.system(f'pipreqs {dep}')            
        except:
            print(f'Please run in the {self.project} directory')

    # deletes your entire project
    def destroy(self):
        try:
            cwd = os.getcwd()
            project_dir = os.path.join(cwd, self.project)
            shutil.rmtree(project_dir)
            print("Project deleted")
        except:
            print(f"Please run from the directory {self.project} was placed in")

    def help_me(self):
        print("""
        \tusage: pb.py <option> <project-name>

        \toptions:
        \tinit - Builds project file structure
        \tbuild - Builds an executable of the main.py file
        \tdestroy - Deletes all project files
        \tdep - Creates a requirements file for the dependencies in main.py

        \tFor more information you can read the README.md file
        """)

command = argv[1]

try:
    project_name = ProjectCreator(argv[2])
except:
    pass

if command.lower() == 'init':
    project_name.init() 
elif command.lower() == 'destroy':
    project_name.destroy()
elif command.lower() == 'build':
    project_name.build()
elif command.lower() == 'dep':
    project_name.dependencies()
elif command.lower() == 'help':
    project_name.help_me()
else:
    print("Command not found.")
